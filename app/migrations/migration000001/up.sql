-- SQL for migration000001 up.


-- Creating user Table

CREATE TABLE user (
	id INT AUTO_INCREMENT NOT NULL,
	username VARCHAR(100) NOT NULL,
	email VARCHAR(200) NOT NULL,
	password VARCHAR(300) NOT NULL,
	enabled TINYINT(1) NOT NULL,
	roles LONGTEXT NOT NULL,
	created_date_time DATETIME DEFAULT NULL,
	last_update_date_time DATETIME DEFAULT NULL,
	PRIMARY KEY(id),
	UNIQUE INDEX username_unique (username),
	UNIQUE INDEX email_unique (email)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;


-- Adding Index to User Table
CREATE INDEX search_idx ON user (username, email, created_date_time);

-- Creating code table
CREATE TABLE code (
	id BIGINT AUTO_INCREMENT NOT NULL,
	value VARCHAR(30) NOT NULL,
	used TINYINT(1) NOT NULL,
	read_count INT NOT NULL DEFAULT 0,
	created_date_time DATETIME DEFAULT NULL,
	last_update_date_time DATETIME DEFAULT NULL,
	created_by INT DEFAULT NULL,
	last_accessed_by INT DEFAULT NULL,
	PRIMARY KEY(id),
	UNIQUE INDEX code_value_unique (value)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;

ALTER TABLE code ADD CONSTRAINT FK_created_by_user FOREIGN KEY (created_by) REFERENCES user (id);
ALTER TABLE code ADD CONSTRAINT FK_last_accessed_by_user FOREIGN KEY (last_accessed_by) REFERENCES user (id);

-- Adding Index to code Table
CREATE INDEX search_idx ON code (value, used, created_date_time, created_by);
