-- SQL for migration000001 rollback.

ALTER TABLE code DROP FOREIGN KEY FK_created_by_user;
ALTER TABLE code DROP FOREIGN KEY FK_last_accessed_by_user;
DROP TABLE code;
DROP TABLE user;