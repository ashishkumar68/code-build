<?php

/**
 *  File for Defining parameters for application.
 *
 *  @author Ashish Kumar
 */

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'mindfire');
define('DB_NAME', 'code_build');
define('DB_PORT', 3306);
define('CODE_EXPORT_DIR', '/var/www/code-build/web/documents/exports/code/');
define('CODE_MIN_GEN', 45000);
define('CODE_BATCH_SIZE', 2000);