<?php
/**
 *  Routing file for handling controller redirection of requests and all the middleware functions.
 *
 *  @author Ashish Kumar
 */

require_once('../src/Service/AuthenticateAuthorizeService.php');
require_once('../src/Service/ApiResponseService.php');

class Routing
{
	public static $routes = [
		'/api/1.0/code/generate/' => [
			'file' => '../src/Controller/API/V10/CodeController.php',
			'class' => 'CodeController',
			'action' => 'generateCodes',
			'method_allowed' => 'POST',
			'resource' => 'code',
		],
		'/api/1.0/code/list/export' => [
			'file' => '../src/Controller/API/V10/CodeController.php',
			'class' => 'CodeController',
			'action' => 'exportCodeList',
			'method_allowed' => 'GET',
			'resource' => 'code',
		],
		'/api/1.0/code/list/' => [
			'file' => '../src/Controller/API/V10/CodeController.php',
			'class' => 'CodeController',
			'action' => 'codeList',
			'method_allowed' => 'GET',
			'resource' => 'code',
		],
		'/api/1.0/code/' => [
			'file' => '../src/Controller/API/V10/CodeController.php',
			'class' => 'CodeController',
			'action' => 'codeStatus',
			'method_allowed' => 'GET',
			'resource' => 'code',
		],
		'/api/1.0/user/login' => [
			'file' => '../src/Controller/API/V10/UserController.php',
			'class' => 'UserController',
			'action' => 'loginUser',
			'method_allowed' => 'POST',
			'resource' => 'user',
		],
		'/api/1.0/user/logout' => [
			'file' => '../src/Controller/API/V10/UserController.php',
			'class' => 'UserController',
			'action' => 'logoutUser',
			'method_allowed' => 'POST',
			'resource' => 'user',
		],
	];

	/**
	 *  Function to Call the actual controller function corresponding to the matched routes.
	 * 
	 *  @return void
	 */
	public static function callConcernedController()
	{
		$controllerData = null;
		$routeUri = null;
		$requestUri = $_SERVER['REQUEST_URI'];
		foreach (self::$routes as $route => $data) {
			if (0 === strpos($_SERVER['REQUEST_URI'], $route)) {
				$routeUri = $route;
				$controllerData = $data;
				break;
			}
		}

		$apiResponseService = new ApiResponseService();

		if (empty($controllerData)) {
		    $apiResponseService->createErrorResponse(404,
                'Resource request was not found on server.')
            ;

		} elseif ($_SERVER['REQUEST_METHOD'] !== $controllerData['method_allowed']) {
            $apiResponseService->createErrorResponse(405,
                'Request method not Allowed.')
            ;
		}

		try {
            if ('CodeController' === $controllerData['class']) {
                $authService = new AuthenticateAuthorizeService();
                $authService->authenticateRequest();

                $authService->authorizeRequest($controllerData['resource'], $_SERVER['REQUEST_METHOD']);
            }

            $request = [
                'method' => $_SERVER['REQUEST_METHOD'],
                'scheme' => $_SERVER['REQUEST_SCHEME'],
                'request_uri' => $_SERVER['REQUEST_URI'],
            ];

            // including controller file
            require_once($controllerData['file']);
            $controllerClass = new $controllerData['class'];

            $data = str_replace($routeUri, '', $requestUri);
            $params = explode("/", trim($data));
            call_user_func([$controllerClass, $controllerData['action']], $request, $params);
        } catch (\Exception $ex) {
		    $apiResponseService->createErrorResponse(500, 'Internal Server Error Occurred.');
        }
	}
}