<?php
/**
 *  Service Class for generating Api Responses.
 *
 *  @author Ashish Kumar
 */

class ApiResponseService
{
    /**
     * Function to generate Error Response to be returned from API.
     *
     *  @param $statusCode
     *  @param $statusMessage
     *
     */
    public function createErrorResponse($statusCode, $statusMessage)
    {
        $response = [
            'reasonCode' => '1',
            'reasonText' => 'Failure',
            'error' => [
                'errorCode' => $statusCode,
                'errorText' => $statusMessage
            ]
        ];

        http_response_code($statusCode);
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
}