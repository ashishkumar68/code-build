<?php
/**
 *  Service Class for handling Auth Related functionality for API Requests.
 *
 *  @author Ashish Kumar
 */

require_once('../src/Repository/UserRepository.php');
require_once('../app/config/config.php');
require_once('../src/Security/RoleResourcePermission.php');
require_once('../src/Service/ApiResponseService.php');

class AuthenticateAuthorizeService
{
	/**
	 *  Function to authenticate the API request.
	 *
	 *  @return array
	 */
	public function authenticateRequest()
	{
		$user = null;
		if (!empty($_SESSION['username'])) {
			$connection = new PDO("mysql:host=localhost;dbname=".DB_NAME, DB_USER, DB_PASS);
			$userRepo = (new UserRepository())->setConnection($connection);
			$user = $userRepo->fetchUser($_SESSION['username']);
		}

		$apiResponseService = new ApiResponseService();

		// Checking if user exists or not.
		if (empty($user)) {
		    $apiResponseService
                ->createErrorResponse(401, 'Request failed to be authenticated.');
		}

		return $user;
	}

	/**
	 *  Function to authorize the API request user.
	 *
	 *  @param string $resourceName
	 *  @param string $operation
	 *  
	 *  @return void
     *  @throws \Exception
	 */
	public function authorizeRequest($resourceName, $operation)
	{
		$permissionsMap = null;
		if (!empty($_SESSION['roles'])) {
			$permissionMap = RoleResourcePermission::getRoleResourcePermission();
		}

        $apiResponseService = new ApiResponseService();

		if (!in_array($operation, $permissionMap[$resourceName])) {
            $apiResponseService->createErrorResponse(403, 'Request failed to be authorized.');
		}
	}
}