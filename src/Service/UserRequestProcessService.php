<?php
/**
 *  Service for processing User module related API requests.
 *
 *  @author Ashish Kumar
 */

require_once('../src/Repository/UserRepository.php');
require_once('../src/Service/ApiResponseService.php');
require_once('../app/config/config.php');

class UserRequestProcessService
{
	/**
	 *  Function to process Login user request.
	 *
	 *  @param $data
	 *
	 *  @return array
     *  @throws \Exception
	 */
	public function processLoginUserRequest($data)
	{
		$processResult['status'] = false;
		try {
            $apiResponseService = new ApiResponseService();
			if (empty($data['username']) || empty($data['password'])) {
				throw new \Exception('Invalid Credentials provided.');
			}

			$connection = new PDO("mysql:host=localhost;dbname=".DB_NAME, DB_USER, DB_PASS);
			$userRepo = (new UserRepository())->setConnection($connection);
			$user = $userRepo->fetchUser($data['username'], $data['password']);

			if (empty($user) || !$user['enabled']) {
                $apiResponseService
                    ->createErrorResponse(401, 'Request failed to be authenticated.');
			}

            $_SESSION['id'] = $user['id'];
			$_SESSION['username'] = $user['username'];
			$_SESSION['roles'] = explode(",", $user['roles']);

			$processResult['message']['response'] = [
				'user' => $user
			];
			$processResult['status'] = true;
		} catch (\Exception $ex) {
			// log the exception here.
			throw $ex;
		}

		return $processResult;
	}

	/**
	 *  Function to process Logout user request.
	 *
	 *  @return array
     *  @throws \Exception
	 */
	public function processLogoutUserRequest()
	{
		$processResult['status'] = false;
		try {
			$processResult['status'] = session_destroy();
		} catch (\Exception $ex) {
			//log the exception here.
			throw $ex;
		}

		return $processResult;
	}
}