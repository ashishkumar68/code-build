<?php
/**
 *  Service to process all the code module related API requests.
 *
 *  @author Ashish Kumar
 */

require_once('../src/Repository/CodeRepository.php');
require_once('../app/config/config.php');

class CodeRequestProcessService
{
	/**
	 *  Function to generate Codes as per provided parameters.
	 *  
	 *  @param integer $count
	 *  @param integer $userId
	 * 
	 *  @return array
	 *  @throws \Exception
	 */
	public function generateCodes($count, $userId)
	{
		$generateResult['status'] = false;
		try {

			// Checking $count input.
			if (empty($count) || !ctype_digit($count) || $count <= CODE_MIN_GEN) {
				throw new \Exception('Invalid Count Parameter value provided.');
			}

			$codes = [];

			for ($val = 0; $val < $count; $val++) {
				$codes[] = $this->generateNewCode();
			}

            $connection = new PDO("mysql:host=localhost;dbname=".DB_NAME, DB_USER, DB_PASS);
            $codeRepo = (new CodeRepository())->setConnection($connection);
            // Validate Generated Codes
            $codes = $this->validateGeneratedCodes($codes, $count, $codeRepo);
            reset($codes);

            // Create records.
            $createdDateTime = (new \DateTime('now', new \DateTimeZone('UTC')))->format('Y-m-d H:i:s');
            $lastUpdateDateTime = $createdDateTime;

            $codeAttributes = ['value', 'read_count', 'created_date_time', 'last_update_date_time', 'created_by'];
            $records = [];
            $codesCount = count($codes);
            for ($val = 0; $val < $codesCount; $val++) {
                $records[] = [
                    $codes[$val], 0, $createdDateTime, $lastUpdateDateTime, $userId
                ];
            }

			// Saving codes in DB.
			$this->saveCodesInDB($records, $codeAttributes, $codeRepo);
			// Generating and exporting the codes File.
			$this->generateCodesFile($codes);

			$generateResult['status'] = true;
		} catch (\Exception $ex) {
			// log exception here

			throw $ex;
		}

		return $generateResult;
	}

	/**
	 *  Function to save generated codes in DB.
	 *
	 *  @param array $codes
	 *  @param array $codeAttributes
     *  @param CodeRepository $codeRepo
	 *  
	 *  @return array
     *  @throws \Exception
	 */
	public function saveCodesInDB($codes, $codeAttributes, $codeRepo)
	{
		$saveResult['status'] = false;
        $connection = $codeRepo->getConnection();
		try {
			// Validate Codes input
			if (!is_array($codes)) {
				throw new \Exception('Invalid input codes passed to the function as parameter.');
			}


			// Starting a Transaction
            $connection->query( "START TRANSACTION");
            $connection->query( "SET AUTOCOMMIT = 0;");

			$codeBatch = [];
			while (count($codes) !== 0) {
				$codeBatch[] = array_pop($codes);
				// iterating over batches and saving
				if (count($codeBatch) % CODE_BATCH_SIZE === 0) {
					$insertStatus = $codeRepo->multiInsert($codeAttributes, $codeBatch, 'code');

					if (false === $insertStatus) {
						throw new Exception($connection->errorCode());
					}

					$codeBatch = [];
				}
			}

			if (count($codeBatch) > 0) {
				$insertStatus = $codeRepo->multiInsert($codeAttributes, $codeBatch, 'code');

				if (false === $insertStatus) {
                    throw new Exception($connection->errorCode());
				}
			}

            $connection->query( "COMMIT");
            $saveResult['status'] = true;
		} catch (\Exception $ex) {
            $connection->query( "ROLLBACK");
            // log exception here.
			throw $ex;
		}

		return $saveResult;
	}

	/**
	 *  Function to export codes in a csv file.
	 *
	 *  @param array $codes
     *
	 *  @return void
	 *  @throws \Exception
	 */
	public function generateCodesFile($codes)
	{
	    $headers = ['code'];
		$data = array_merge($headers, $codes);
		$fileName = 'codes_'.time().'.csv';
		
		// Creating Directory if it doesn't exist already.
		if (!is_dir(CODE_EXPORT_DIR)) {
			mkdir(CODE_EXPORT_DIR, 0744, TRUE);
		}

		// Saving content into a file
		file_put_contents(CODE_EXPORT_DIR.$fileName, implode(PHP_EOL, $data));
		// exporting created file.
		$this->exportFile(CODE_EXPORT_DIR.$fileName);
	}

	/**
	 *  Function to export a file as a response.
	 *
	 *  @param $file
	 *
	 *  @return void
	 *  @throws \Exception
	 */
	public function exportFile($file)
	{
		if (!file_exists($file)) {
			throw new \Exception('Export File not found.');
		} else {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($file).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));

			readfile($file);
			exit;
		}
	}

	/**
	 *  Function to Process GET code Status request.
	 *  
	 *  @param string $code
	 *  @param integer $user
	 * 
	 *  @return array
     *  @throws \Exception
	 */
	public function processGetCodeStatusRequest($code, $user)
	{
		$processResult['status'] = false;
		$connection = NULL;
		try {
			$connection = new PDO("mysql:host=localhost;dbname=".DB_NAME, DB_USER, DB_PASS);
			$codeRepo = (new CodeRepository())->setConnection($connection);
			$connection->query("START TRANSACTION");
            $connection->query("SET AUTOCOMMIT=0");
			$code = $codeRepo->getCodeRecord($code, true);

			if (!empty($code)) {
				$codeRepo->updateCodeReadCount($code['code'], $user);
			} else {
				throw new \Exception("Code : $code not found in Database");
			}

			$connection->query("COMMIT");
			// returning the increased read count.
			$code['read_count'] += 1;
			$processResult['message']['response'] = $code;
			$processResult['status'] = true;
		} catch (\Exception $ex) {
			$connection->query("ROLLBACK");
			// log exception here

			throw $ex;
		}

		return $processResult;
	}

	/**
	 *  Function to get the codes list page records.
	 *
	 *  @param $pagination
     *
	 *  @return array
     *  @throws \Exception
	 */
	public function processGetCodeListRequest($pagination)
	{
		$processResult['status'] = false;
		try {
			$pagination = json_decode(base64_decode($pagination), TRUE);

			$page = (!empty($pagination['pagination']['page'])
                && (ctype_digit($pagination['pagination']['page']) || is_int($pagination['pagination']['page'])))
				? $pagination['pagination']['page']
				: 1
			;

			$limit = (!empty($pagination['pagination']['limit']) && ctype_digit($pagination['pagination']['limit'])) 
				? $pagination['pagination']['limit'] 
				: 10
			;

			$connection = new PDO("mysql:host=localhost;dbname=".DB_NAME, DB_USER, DB_PASS);
			$codeRepo = (new CodeRepository())->setConnection($connection);

			$processResult['message']['response'] = [
				'codes' => $codeRepo->fetchCodeList($page, $limit),
				'count' => $codeRepo->getTotalCodeRecords(),
			];
			$processResult['status'] = true;
		} catch (\Exception $ex) {
		    // log exception here
			throw $ex;
		}

		return $processResult;
	}

	/**
	 *  Function to process Get code list export request.
	 *
	 *  @return mixed
     *  @throws \Exception
	 */
	public function processGetCodeListExportRequest()
	{
		$processResult['status'] = false;
		try {
			$connection = new PDO("mysql:host=localhost;dbname=".DB_NAME, DB_USER, DB_PASS);
			$codeRepo = (new CodeRepository())->setConnection($connection);

			$totalCodes = $codeRepo->getTotalCodeRecords();
			$pages = ceil($totalCodes / CODE_BATCH_SIZE);
			$headers = ['code', 'read count'];

			$file = CODE_EXPORT_DIR.'read_code_export_'.time().'.csv';

			file_put_contents($file, implode(",", $headers).PHP_EOL);

			for ($page = 1; $page <= $pages; $page++) {
				$codes = $codeRepo->fetchCodeList($page, CODE_BATCH_SIZE);
				$codesCount = count($codes);
				$fileRecords = [];
				for ($record = 0; $record < $codesCount; $record++) { 
					$fileRecords[] = implode(",", $codes[$record]);
				}
				
				file_put_contents($file, implode(PHP_EOL, $fileRecords), FILE_APPEND);
			}

			// exporting File.
			$this->exportFile($file);

			$processResult['status'] = true;
		} catch (\Exception $ex) {
            // log exception here
			throw $ex;
		}

		return $processResult;
	}

    /**
     *  Function to generate a new (Most Probably Unique) Transaction Number
     *  for creating slug fields.
     *
     *  @param integer $length
     *
     *  @return string
     */
    public function generateNewCode($length = 14)
    {
        $currentTime = explode(" ", microtime());
        $code = $currentTime[1] . substr($currentTime[0], 2, 8);
        return substr(sha1($code . rand(0, 100) . rand(20, 787)),  -1 * $length);
    }

    /**
     *  Function to validate and update the duplicate codes.
     *
     *  @param array $codes
     *  @param integer $requiredCodeCount
     *  @param CodeRepository $codeRepo
     *
     *  @return array
     */
    public function validateGeneratedCodes($codes, $requiredCodeCount, $codeRepo)
    {
        $codes = array_unique($codes);

        $validCodes = [];
        while (0 !== ($codeCount = count($codes)) && count($validCodes) !== $requiredCodeCount) {
            $batchCodes = [];
            // Pulling the codes batch of size code batch Size.
            if ($codeCount < CODE_BATCH_SIZE) {
                $batchCodes = array_flip($codes);
                $codes = [];
            } else {
                while (count($batchCodes) !== CODE_BATCH_SIZE) {
                    $code = array_pop($codes);
                    $batchCodes[$code] = 1;
                }
            }

            $invalidCodes = $codeRepo->filterAvailableCodes(array_keys($batchCodes));

            // Removing Invalid Codes from Batch Codes.
            foreach ($invalidCodes as $code) {
                unset($batchCodes[$code]);
            }

            // Merging Valid Codes to its Array
            $validCodes = array_merge($validCodes, array_keys($batchCodes));

            $invalidCodesCount = count($invalidCodes);

            // Generating more codes to replace the invalid ones.
            for ($code = 0; $code < $invalidCodesCount ; $code++) {
                $codes[] = $this->generateNewCode();
            }
        }

        return $validCodes;
    }
}