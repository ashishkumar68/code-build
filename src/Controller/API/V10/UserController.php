<?php
/**
 *  UserController for handling user module related requests.
 *
 *  @author Ashish Kumar
 */

require_once('../src/Service/UserRequestProcessService.php');
require_once('../src/Service/ApiResponseService.php');

class UserController
{
	/**
	 *  Function to handle User Login request.
	 *  
	 *  @route POST /user/login
     *
	 *  @param $request
     *  @param $data
	 *
	 *  @return void
	 */
	public function loginUser($request, $data)
	{
	    try {
            $userRequestProcessService = new UserRequestProcessService();
            header('Content-Type: application/json');
            echo json_encode($userRequestProcessService->processLoginUserRequest($_POST)
            ['message']['response']['user'])
            ;
            exit;
        } catch (\Exception $ex) {
            // log exception here
            $apiResponseService = new ApiResponseService();
            $apiResponseService->createErrorResponse(500, 'Internal Server Error Occurred.');
        }
	}

	/**
	 *  Function to handle User Login request.
	 *  
	 *  @route POST /user/logout
	 *  @param $request
     *  @param $data
	 *
	 *  @return void
	 */
	public function logoutUser($request, $data)
	{
	    try {
            $userRequestProcessService = new UserRequestProcessService();
            header('Content-Type: application/json');
            echo json_encode($userRequestProcessService->processLogoutUserRequest());
            exit;
        } catch (\Exception $ex) {
            // log exception here
            $apiResponseService = new ApiResponseService();
            $apiResponseService->createErrorResponse(500, 'Internal Server Error Occurred.');
        }
	}
}