<?php
/**
 *  CodeController file for handling Code Module related requests.
 *
 *  @author Ashish Kumar
 */

require_once('../src/Service/CodeRequestProcessService.php');
require_once('../src/Service/ApiResponseService.php');

class CodeController
{
	/**
	 *  Function to generate Codes as per the input number
	 *  provided to the url.
	 *  @route POST /code/generate/{number}
	 *
	 *  @param $request
	 *  @return void
	 */
	public function generateCodes($request, $data)
	{
	    try {
            $number = is_array($data) ? $data[0] : $data;
            $codeRequestProcessService = new CodeRequestProcessService();
            $codeRequestProcessService->generateCodes($number, $_SESSION['id']);
        } catch (\Exception $ex) {
	        // log exception here
            $apiResponseService = new ApiResponseService();
            $apiResponseService->createErrorResponse(500, 'Internal Server Error Occurred.');
        }
	}

	/**
	 *  Function to fetch/read Code Status.
	 *  @route GET /code/{code}
	 *
	 *  @param $request
	 *  @return string
	 */
	public function codeStatus($request, $data)
	{
	    try {
            $code = is_array($data) ? $data[0] : $data;
            $codeRequestProcessService = new CodeRequestProcessService();
            header("Content-Type: application/json");
            echo json_encode($codeRequestProcessService
                ->processGetCodeStatusRequest($code, $_SESSION['id'])['message']);
            ;
            exit;
        } catch (\Exception $ex) {
            // log exception here
            $apiResponseService = new ApiResponseService();
            $apiResponseService->createErrorResponse(500, 'Internal Server Error Occurred.');
        }
	}

	/**
	 *  Function to fetch the list of read codes
	 *  @route GET /code/list/{data}
	 *  
	 *  @param $request
	 *  @param $data
	 *  @return string
	 */
	public function codeList($request, $data)
	{
	    try {
            $data = is_array($data) ? $data[0] : $data;
            $codeRequestProcessService = new CodeRequestProcessService();
            header("Content-Type: application/json");
            echo json_encode($codeRequestProcessService
                ->processGetCodeListRequest($data)['message']['response']);
            ;
            exit;
        } catch (\Exception $ex) {
            $apiResponseService = new ApiResponseService();
            $apiResponseService->createErrorResponse(500, 'Internal Server Error Occurred.');
        }
	}

	/**
	 *  Function to fetch the read codes file
	 *  to be returned to the response.
	 *  @route GET /code/list/export
	 *
	 *  @param $request
     *  @param $data
	 *  @return void
	 */
	public function exportCodeList($request, $data)
	{

	    try {
            $codeRequestProcessService = new CodeRequestProcessService();
            $codeRequestProcessService->processGetCodeListExportRequest();
        } catch (\Exception $ex) {
            $apiResponseService = new ApiResponseService();
            $apiResponseService->createErrorResponse(500, 'Internal Server Error Occurred.');
        }
	}
}