<?php
/**
 *  Class CodeRepository for performing DB Queries for Code module and creating results.
 *
 *  @author Ashish Kumar
 */

class CodeRepository
{
    /**
     * @var PDO
     */
	private $connection;

	/**
	 *  UserRepository Constructor
	 * 
	 *  @param $conn
	 *  @return void
	 */ 
	public function __constuct($conn)
	{
		$this->connection = $conn;
	}

    /**
     *  Function to set Connection property.
     *
     *  @param PDO $connection
     *  @return CodeRepository
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;

        return $this;
    }

    /**
     *  Function to get connection object.
     *  
     *  @return PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

	/**
     *  Function to Multi Insert vouchers into Voucher Table.
     *
     * @param $attributes
     * @param $importData
     * @param $tableName
     *
     * @return PDOStatement
     * @throws \Exception
     */
    public function multiInsert($attributes, $importData, $tableName)
    {
        if ($importData && count($importData) === 1) {
            $query = 'INSERT INTO ' . $tableName . ' (' . implode(',', array_values($attributes)) .
                ') VALUES ' . ($this->createArrayForVoucherEntries($importData)[0]);
            return $this->connection->query($query);
        }

        $query = 'INSERT INTO ' . $tableName . ' (' . implode(',', array_values($attributes)) .
            ') VALUES ' . implode(',', $this->createArrayForVoucherEntries($importData));

        return $this->connection->query($query);
    }

    /**
     *  Function Create the array of table entries for Import
     *
     * @param $importData
     *
     * @return array
     */
    public function createArrayForVoucherEntries($importData)
    {
        $voucherEntries = [];
        // Iterating on Import data and fill Vouchers Entries
        foreach ($importData as $data) {
            $voucherEntries[]= '("' . implode('","', array_values($data)) . '")';
        }
        return $voucherEntries;
    }

    /**
     *  Function to select and lock code record before update.
     *
     *  @param string $code
     *  @param boolean $lock (default = false)
     *
     *  @return array
     */
    public function getCodeRecord($code, $lock = false)
    {
        $lockQueryString = (true === $lock) ? ' FOR UPDATE ' : null;
        $result = $this->connection->prepare('SELECT value as code, used, read_count 
          FROM code WHERE value = :code'.$lockQueryString);

        $result->execute([':code' =>$code]);
        $code = null;

        while ($data = $result->fetch()) {
            $code = [
                'code' => $data['code'],
                'used' => $data['used'],
                'read_count' => $data['read_count'],
            ];
        }

        return $code;
    }

    /**
     *  Function to update code read count.
     *
     *  @param $code
     *  @param $userId
     *
     *  @return PDOStatement
     */
    public function updateCodeReadCount($code, $userId)
    {
        return $this->connection->query("UPDATE code SET used = 1, read_count = (read_count + 1), 
            last_accessed_by = $userId WHERE value = '$code'");
    }

    /**
     *  Function to fetch code records as per provided
     *  Page parameters.
     *
     *  @param integer $page
     *  @param integer $limit
     *
     *  @return array
     */
    public function fetchCodeList($page, $limit)
    {
        $offset = ($page - 1) * $limit;

        $result = $this->connection->query("SELECT value as code, read_count FROM code WHERE read_count > 0 
            LIMIT $offset, $limit");

        $codes = [];

        while ($data = $result->fetch()) {
            $codes[] = [
                'code' => $data['code'],
                'readCount' => $data['read_count'],
            ];
        }

        return $codes;
    }

    /**
     *  Function to fetch the number of code records.
     *
     *  @param boolean $readRecords
     *  @return integer
     */
    public function getTotalCodeRecords($readRecords = true)
    {
        $readRecordsCondition = (true === $readRecords) ? ' WHERE read_count > 0 ' : null;
        $result = $this->connection->query("SELECT count(id) as total_codes FROM code $readRecordsCondition");
        $totalCodes = 0;

        while ($data = $result->fetch()) {
            $totalCodes = $data['total_codes'];
        }

        return $totalCodes;
    }

    /**
     *  Function to filter available codes from the provided codes list.
     *
     *  @param array $codes
     *
     *  @return array
     */
    public function filterAvailableCodes($codes)
    {
        $availableCodes = [];

        $searchString = implode("','", $codes);

        $result = $this->connection->query("SELECT value as code FROM code WHERE value IN (':codes')");
        $result->bindParam('codes', $searchString);

        while ($data = $result->fetch()) {
            $availableCodes[] = $data['code'];
        }

        return $availableCodes;
    }
}