<?php
/**
 * Class UserRepository for performing DB queries for User module and creating results.
 *
 */

class UserRepository
{
    /**
     * @var PDO
     */
	private $connection;

	/**
	 *  UserRepository Constructor
	 * 
	 *  @param $connection
	 *  @return void
	 */ 
	public function __constuct($connection)
	{
		$this->connection = $connection;
	}

	/**
     *  Function to set Connection property.
     *
     *  @param PDO $connection
     *
     *  @return UserRepository
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;

        return $this;
    }

    /**
     *  Function to get connection object.
     *  
     *  @return PDO Connection object
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     *  Function to fetch username and roles of user while login.
     *
     *  @param string $username
     *  @param string $password (default = '')
     *
     *  @return array
     */
    public function fetchUser($username, $password = '')
    {
    	$params = [
    		':username' => $username
    	];
    	
    	$passwordCondition = null;

    	if (!empty($password)) {
    		$passwordCondition .= "AND password = :password";
    		$params[':password'] = sha1($password);
    	}

    	$statement = $this->getConnection()
    		->prepare("SELECT id, username, roles, enabled FROM user WHERE username = :username $passwordCondition");

    	$statement->execute($params);

    	$row = $statement->fetch();
		$user = null;

    	if (!empty($row)) {
    		$user = [
    		    'id' => $row['id'],
    			'username' => $row['username'],
    			'roles' => $row['roles'],
    			'enabled' => $row['enabled']
    		];
    	}

    	return $user;
    }
}