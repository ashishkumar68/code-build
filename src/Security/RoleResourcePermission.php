<?php
/**
 *  Class RoleResourcePermission for Authorization handling for API requests.
 *
 *  @author Ashish Kumar
 */

class RoleResourcePermission
{
	const RESOURCE_CODE = 'code';
	const RESOURCE_USER = 'user';

	const ACCESS_VIEW = 'GET';
	const ACCESS_CREATE = 'POST';
	const ACCESS_EDIT = 'PUT';
	const ACCESS_DELETE = 'DELETE';
	const ACCESS_MASTER = 'MASTER';

	const ROLE_ADMIN = 'ROLE_ADMIN';
	const ROLE_CUSTOMER_CARE = 'ROLE_CUSTOMER_CARE';

	const ROLE_CODE_READER = [
		self::RESOURCE_CODE. '_' . self::ACCESS_VIEW
	];

	const ROLE_CODE_ADMIN = [
		self::RESOURCE_CODE. '_' . self::ACCESS_VIEW,
		self::RESOURCE_CODE. '_' . self::ACCESS_CREATE,
		self::RESOURCE_CODE. '_' . self::ACCESS_EDIT
	];

	const ROLE_USER_READER = [
		self::RESOURCE_USER. '_' . self::ACCESS_VIEW
	];

	const ROLE_USER_ADMIN = [
		self::RESOURCE_USER. '_' . self::ACCESS_VIEW,
		self::RESOURCE_USER. '_' . self::ACCESS_CREATE,
		self::RESOURCE_USER. '_' . self::ACCESS_EDIT
	];

	public static $resourcePermissions = [
		self::ROLE_ADMIN => [
			self::ROLE_CODE_ADMIN, self::ROLE_USER_READER
		],
		self::ROLE_CUSTOMER_CARE => [
			self::ROLE_CODE_ADMIN, self::ROLE_USER_READER
		],
	];

	/**
	 *  Function to get the roles resoource permissions map.
	 *
	 *	@return array
     *  @throws \Exception
	 */
	public static function getRoleResourcePermission()
	{
		$roles = $_SESSION['roles'];

		$permissions = [];
		$permissionMap = [];

        // Fetching Permissions Assigned For User's Role.
        foreach ($roles as $role) {
        	if (!in_array($role, [self::ROLE_ADMIN, self::ROLE_CUSTOMER_CARE])) {
        		throw new \Exception('Role not supported');	
        	}
            // Getting Assigned permissions for user's Role.
            $assignedRolePermissions = self::$resourcePermissions[$role];
            $permissions = array_merge($permissions, array_merge(...$assignedRolePermissions));
        }

        // Iterating through Permissions and creating Resource Permissions Map.
        foreach ($permissions as $permission) {
            $resourcePermission = explode("_", $permission);
            if (isset($permissionMap[$resourcePermission[0]])) {
                $permissionMap[$resourcePermission[0]][] = $resourcePermission[1];
            } else {
                $permissionMap[$resourcePermission[0]] = [$resourcePermission[1]];
            }
        }

        return $permissionMap;
	}
}