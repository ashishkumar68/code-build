<?php

require_once('../app/config/routing.php');

if (0 === strpos($_SERVER['REQUEST_URI'], '/api')) {
	Routing::callConcernedController();
	exit;
}

if (!empty($_SESSION['username']) && !empty($_SESSION['roles'])) {
	header('location: dashboard.html');
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Code-Gen App</title>
	<link rel="stylesheet" href="/plugin/bootstrap/css/bootstrap.min.css"/>
</head>
<body>
	<br><br><br><br><br><br><br><br>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="row">
									<h3>Login</h3>
								</div><br>
								<div class="row">
									<form class="form-horizontal" id="loginForm">
										<div class="form-group">
											<label for="username" class="col-sm-2 control-label">Username</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" name="username" id="username" placeholder="Enter your Username" required>
											</div>
										</div>
										<div class="form-group">
											<label for="password" class="col-sm-2 control-label">Password</label>
											<div class="col-sm-10">
												<input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" required>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
												<button type="submit" class="btn btn-default">Sign in</button>
											</div>
										</div>
									</form>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script type="text/javascript" src="/plugin/jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/plugin/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/plugin/jquery/jquery.toaster.js"></script>
    <script type="text/javascript" src="/plugin/loader/loadingoverlay.min.js"></script>
    <script type="text/javascript" src="/js/userModule.js"></script>
</body>
</html>