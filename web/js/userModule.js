var userModule = (function () {
    'use strict';

    /**
     * Function to make Ajax request to server and process response.
     *
     * @param async
     * @param url
     * @param verb
     * @param contentType
     * @param data
     * @param successCallback
     * @param errorCallback
     *
     * @return void
     */
    var sendServerRequest = function(async, url, verb, contentType, data, successCallback, errorCallback) {

        if (null !== data) {
            // Appending base64_encoded data in url if http verb is a GET request.
            if ('GET' === verb) {
                url += "/"+ btoa(JSON.stringify(data));
                data = null;
            } else if ('application/json' === contentType) {
                // Else Json stringify the data.
                data = JSON.stringify(data);
            }
        }

        // AJAX call to the Server url with provided data.
        $.ajax({
            type: verb,
            url: url,
            data: data,
            processData: false,
            contentType: contentType,
            async: async,
            beforeSend: function () {
                // Showing Loader
                $('body').LoadingOverlay("show", {
                    image       : "",
                    fontawesome : "fa fa-spinner fa-spin"
                });
            },
            success: function (data) {
                successCallback(data);
            },
            error: function (err) {
                $('body').LoadingOverlay("hide");
                errorCallback(err);
            },
            complete: function (response) {
                // Hiding Loader
                $('body').LoadingOverlay("hide");
                if (401 === response.status) {
                    showFlashMessage('danger', 'Invalid Credentials provided.');
                }
            }
        });
    };

    /**
     *  Function to show different Types of Flash Messages.
     *
     *  @param type
     *  @param message
     *
     *  @return void
     */
    var showFlashMessage= function(type, message) {
        var priority = null;
        var title = null;
        switch(type) {
            case 'danger':
                priority = 'danger';
                title = 'Error';
                break;
            case 'warning':
                priority = 'warning';
                title = 'Warning';
                break;
            case 'success':
                priority = 'success';
                title = 'Success';
                break;
            default :
                priority = 'danger';
                title = 'Error';
        }
        $.toaster({
            priority : priority,
            title : title,
            timeout : 5000,
            message : message
        });
    };

    /**
     *  Function to process the login request response.
     *
     *  @param data
     *  @return void
     */
    var processLoginResponse = function (data) {
        showFlashMessage('success', 'You are loggedin Successfully.');
        location.reload();
    };

    /**
     *  Function to process Error Server Response.
     *
     *  @return void
     */
    var processErrorServerResponse = function (data) {
        showFlashMessage('danger', 'Internal Server Occurred, while fetching codes report');
    };

    /**
     *  Function to send login request to server.
     *
     *  @return void
     */
    var sendLoginRequest = function () {

        var credentials = 'username='+$('#username').val() + '&password='+$('#password').val();

        // Sending Login AJAX request to server.
        sendServerRequest(true, '/api/1.0/user/login', 'POST', 'application/x-www-form-urlencoded', credentials,
            processLoginResponse, processErrorServerResponse);
    };

    var init = function () {
        $('#loginForm').on('submit', function (event) {
            event.preventDefault();

            // Send login request to server.
            sendLoginRequest();
        });
    };

    return {
        init : init
    }
})();

$(document).ready(function () {
    userModule.init();
});