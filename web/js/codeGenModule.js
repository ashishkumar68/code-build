var CodeGenModule = (function () {
	'use strict';

	var currentPage = 1;

	/**
     * Function to make Ajax request to server and process response.
     *
     * @param async
     * @param url
     * @param verb
     * @param data
     * @param successCallback
     * @param errorCallback
     *
     * @return void
     */
    var sendServerRequest = function(async, url, verb, data, successCallback, errorCallback) {

        if (null !== data) {
            // Appending base64_encoded data in url if http verb is a GET request.
            if ('GET' === verb) {
                url += "/"+ btoa(JSON.stringify(data));
                data = null;
            } else {
                // Else Json stringify the data.
                data = JSON.stringify(data);
            }
        }

        // AJAX call to the Server url with provided data.
        $.ajax({
            type: verb,
            url: url,
            data: data,
            processData: false,
            contentType: "application/json",
            async: async,
            beforeSend: function () {
                // Showing Loader
                $('body').LoadingOverlay("show", {
                    image       : "",
                    fontawesome : "fa fa-spinner fa-spin"
                });
            },
            success: function (data) {
                successCallback(data);
            },
            error: function (err) {
                $('body').LoadingOverlay("hide");
                errorCallback(err);
            },
            complete: function (response) {
                // Hiding Loader
                $('body').LoadingOverlay("hide");
                if (401 === response.status) {
                    location.href = 'index.php';
                }
            }
        });
    };

    /**
     *  Function to show different Types of Flash Messages.
     *
     *  @param type
     *  @param message
     *
     *  @return void
     */
    var showFlashMessage= function(type, message) {
        var priority = null;
        var title = null;
        switch(type) {
            case 'danger':
                priority = 'danger';
                title = 'Error';
                break;
            case 'warning':
                priority = 'warning';
                title = 'Warning';
                break;
            case 'success':
                priority = 'success';
                title = 'Success';
                break;
            default :
                priority = 'danger';
                title = 'Error';
        }
        $.toaster({
            priority : priority,
            title : title,
            timeout : 5000,
            message : message
        });
    };

    /**
     *  Function to process Success Code Report data response.
     *
     *  @return void
     */
    var processSuccessCodeReadResponse = function (data) {
    	var codes = data.codes;
    	var codesCount = codes.length;

    	var recordsTableHtml = '';
    	for (var record = 0; record < codesCount ; record++) {
    		recordsTableHtml += 
    			'<tr>' + 
    				'<td>' + codes[record]['code'] + '</td>' +
    				'<td>' + codes[record]['readCount'] + '</td>' +
    			'<tr>'
    	}

    	$('#readCodes').html(recordsTableHtml);

        if (currentPage === 1) {
            createPagination(data.count, $('#recordsPerPage').val());
        }
    };

    /**
     *  Function to process success code generation response.
     *
     *  @param data
     */
    var processSuccessCodeGenerationResponse = function (data) {
        downloadFileResponse(data, 'codes.csv');
        // Resetting the Code Generator form.
        $('#codeGeneratorForm').trigger('reset');
        showFlashMessage('success', 'Codes Generated Successfully');
    };

    /**
     *  Function to process Error Server Response.
     *
     *  @return void
     */
    var processErrorServerResponse = function (data) {
    	showFlashMessage('danger', 'Internal Server Occurred, while fetching codes report');
    };

    /**
     *  Function to fetch the read codes from server.
     *
     */
    var getCodeReadRecords = function () {
    	var data = {
    		'pagination' : {
    			'page' : currentPage,
    			'limit' : $("#recordsPerPage").val()
    		}
    	};

    	// Sending AJAX request to server to fetch code list records.
    	sendServerRequest(true, '/api/1.0/code/list', 'GET', data, processSuccessCodeReadResponse,
    		processErrorServerResponse);
    };

    /**
     * Function to download a file returned from server Response.
     *
     * @param data
     * @param filename
     * @param mime
     *
     * @return void
     */
    var downloadFileResponse = function(data, filename, mime) {
        var blob = new Blob([data], {type: mime || 'application/octet-stream'});
        if (typeof window.navigator.msSaveBlob !== 'undefined') {
            window.navigator.msSaveBlob(blob, filename);
        }
        else {
            var blobURL = window.URL.createObjectURL(blob);
            var tempLink = document.createElement('a');
            tempLink.style.display = 'none';
            tempLink.href = blobURL;
            tempLink.setAttribute('download', filename);

            if (typeof tempLink.download === 'undefined') {
                tempLink.setAttribute('target', '_blank');
            }

            document.body.appendChild(tempLink);
            tempLink.click();
            document.body.removeChild(tempLink);
            window.URL.revokeObjectURL(blobURL);
        }
    };

    /**
     *  Function to generate Codes.
     *
     *  @return void
     */
    var generateCodes = function (codesCount) {
        // Sending AJAX request to server to fetch code list records.
        sendServerRequest(true, '/api/1.0/code/generate/'+codesCount, 'POST', null,
            processSuccessCodeGenerationResponse, processErrorServerResponse);
    };

    /**
     *  Function to process logout response.
     *
     *  @param data
     *  @return void
     */
    var processLogoutResponse = function (data) {
        showFlashMessage('success', 'You have been logged out successfully.')

        location.href = 'index.php';
    };

    /**
     *  Function to send logout request to server.
     *
     *  @return void
     */
    var sendLogoutRequest = function () {
        // Sending AJAX request to server to fetch code list records.
        sendServerRequest(true, '/api/1.0/user/logout', 'POST', null,
            processLogoutResponse, processErrorServerResponse);
    };

    /**
     * Function to Create Pagination.
     *
     * @param totalRecords
     * @param recordsPerPage
     */
    var createPagination = function (totalRecords, recordsPerPage) {
        var paginationElement = $('#pagination');
        var totalPages = Math.ceil(totalRecords / recordsPerPage);
        var showTotalPages = (totalPages > 0 ? totalPages : 1);
        var visiblePages = (
            (totalPages > 0 ? totalPages : 1) < 7
                ?   (totalPages > 0 ? totalPages : 1)
                : 7
        );

        // Initializing Pagination
        paginationElement.twbsPagination('destroy');
        paginationElement.twbsPagination({
            initiateStartPageClick: false,
            startPage: 1,
            currentPage: currentPage,
            totalPages: showTotalPages,
            visiblePages: visiblePages,
            onPageClick: function (event, page) {
                currentPage = page;
                getCodeReadRecords();
            }
        });
    };

    /**
     * Function to be called on page load.
     *
     * @return void
     */
	var init = function () {

		// Added Different tab section event handler.
		$('ul.nav-pills li a').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
		});

		$('a[name="code-report"]').on('shown.bs.tab', getCodeReadRecords);

		$("#recordsPerPage").on('change', function () {
		    currentPage = 1;
            getCodeReadRecords();
        });
		
		$("#codeGenForm").on('submit', function (event) {
		    event.preventDefault();

		    generateCodes($("#code-count").val());
        });

		$("#logout").on('click', sendLogoutRequest);
	};

	return {
		init : init
	}
})();

$(document).ready(function () {
	CodeGenModule.init();
});